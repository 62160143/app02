package com.supakit.app02

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.supakit.app02.databinding.FragmentHomeBinding
import com.supakit.app02.databinding.FragmentPlusBinding

// TODO: Rename parameter arguments, choose names that match


/**
 * A simple [Fragment] subclass.
 * Use the [PlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusFragment : Fragment() {
    private var _binding: FragmentPlusBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPlusBinding.inflate(inflater,container,false)
        // Inflate the layout for this fragment
        return binding?.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.BtnEqual?.setOnClickListener {
            val num1 = binding?.num1?.text.toString()
            val num2 = binding?.num2?.text.toString()
            val num1Double = num1.toInt()
            val num2Double = num2.toInt()
            var result = num1Double+num2Double
            val action = PlusFragmentDirections.actionPlusFragmentToAnswerFragment2(
                letter = result.toString()
            )
            Log.d("check",result.toString())
            view.findNavController().navigate(action)
        }
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

    }
}